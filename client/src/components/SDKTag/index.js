import React from "react";
import PropTypes from "prop-types";
import { Tag } from "antd";

import "./index.css";


export default function SDKTag(props) {
  const { children, editable = false, onClose } = props;

  return (
    <Tag className="sdk-tag" closable={editable} onClose={onClose}>
      {children}
    </Tag>
  );
}
SDKTag.propTypes = {
  children: PropTypes.node,
  editable: PropTypes.bool,
  onClose: PropTypes.func,
}
