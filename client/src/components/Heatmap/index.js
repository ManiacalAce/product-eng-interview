import ApexChart from "react-apexcharts";
import PropTypes from "prop-types";
import React from "react";


export default class Heatmap extends React.Component {

  static propTypes = {
    series: PropTypes.array.isRequired,
    width: PropTypes.any,
    height: PropTypes.any,
    options: PropTypes.object,
  }

  render() {
    const { series, width, height } = this.props;

    // TODO: Merge with user-provided `options`
    const resolvedOptions = this.getDefaultChartOptions();

    return (
      <ApexChart
        type="heatmap"
        series={series}
        width={width}
        height={height}
        options={resolvedOptions}
      />
    );
  }

  getDefaultChartOptions = () => {
    return {
      chart: {
        height: 700,
        type: "heatmap",
      },
      plotOptions: {
        heatmap: {
          colorScale: {
            // ranges: [
            //   {
            //     from: 0,
            //     to: 10,
            //     color: "#FEF3EE",
            //     name: null,
            //   },
            //   {
            //     from: 10,
            //     to: 20,
            //     color: "#FEE9E0",
            //     name: null,
            //   },
            //   {
            //     from: 20,
            //     to: 35,
            //     color: "#FB6E4E",
            //     name: null,
            //   },
            //   {
            //     from: 35,
            //     to: 60,
            //     color: "#F5543C"
            //   },
            //   {
            //     from: 60,
            //     to: 100,
            //     color: "#F24733"
            //   }
            // ],
          },
        },
      },
      tooltip: {
        // TODO: Clearer tooltip showing the feature that the number represents
        // TODO: Sanitize html for custom input
        // custom: ({series, seriesIndex, dataPointIndex, w}) => {
        //   return `
        //     <div class="heatmap-tooltip">

        //     </div>
        //   `
        // },
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ["#000000"],
          fontWeight: "normal",
        },
      },
      stroke: {
        width: 1,
      },
      theme: {
        monochrome: {
          enabled: true,
          color: "#ff0000",
          shadeTo: "light",
          shadeIntensity: 0.05,
        }
      },
      xaxis: {
        title: {
          text: "To SDK",
          offsetY: 10,
        }
      },
      yaxis: {
        title: {
          text: "From SDK",
          offsetX: 10,
        }
      },
      colors: ["#000000"]
    }
  }
}
