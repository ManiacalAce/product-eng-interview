import React from "react";

import CompetitionAnalysis from "./screens/CompetitionAnalysis"

import "antd/dist/antd.css";
import "./App.css";


function App() {
  return (
    <div className="app">
      <header className="app-header">
        <p>App SDKs - Competitive Matrix</p>
      </header>

      <main>
        <CompetitionAnalysis />
      </main>
    </div>
  );
}

export default App;
