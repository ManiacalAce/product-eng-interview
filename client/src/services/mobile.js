import wretch from "wretch";

import config from "../app/config";
import { stringifyParams } from "../utils/utils";


export const baseRoute = `http://${config.API.HOST}:${config.API.PORT}/sdk`

const api = wretch(baseRoute)


export async function getCompetitiveMatrix(sdkIds) {
  return await api.url("/competitive-matrix").query(
    stringifyParams({ sdk_ids: sdkIds })
  ).get().json()
}


export async function getSDKs(sdkIds = null, searchQuery = null, limit = null) {
  return await api.query(
    stringifyParams({ sdk_ids: sdkIds, q: searchQuery, limit: limit })
  ).get().json()
}
