import qs from "qs";


export function stringifyParams(obj) {
  // Filter out all null/undefined items. The `qs` lib seems to have some quirks around
  // this while dealing with arrays.
  // TODO: Verify lib's quirks
  const cleanEntries = Object.entries(obj).filter(
    ([key, value]) => {
      if ((value === null) || typeof value === "undefined") {
        return false;
      }

      return true;
    }
  )
  const cleanObj = Object.fromEntries(cleanEntries);

  return qs.stringify(cleanObj, { indices: false });
}
