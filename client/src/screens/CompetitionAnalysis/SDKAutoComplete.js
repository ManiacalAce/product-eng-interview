import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { AutoComplete, Input } from "antd";

import * as mobileApi from "../../services/mobile";

import "./SDKAutoComplete.css";


/**
 * Handles completion in the matrix editor.
 * TODO: This could be factored out into a general any-API-backed autocomplete.
 */
export default function SDKAutoComplete(props) {
  const { onSelect } = props;

  const [options, setOptions] = useState([]);

  const handleSelect = (value, option) => {
    if (onSelect) {
      onSelect(option);
    }
  };

  const handleSearch = async (value) => {
    const newOptions = await searchSDKs(value);
    setOptions(newOptions);
  }

  return (
    <AutoComplete
        className="autocomplete"
        options={options}
        onSelect={handleSelect}
        onSearch={handleSearch}
    >
      <Input.Search size="large" placeholder="Search for SDKs..." />
    </AutoComplete>
  );
}
SDKAutoComplete.propTypes = {
  onSelect: PropTypes.func,
};


async function searchSDKs(q) {
  const results = await mobileApi.getSDKs(null, q, 6);
  return results.map(item => ({
    value: item.name,
    ...item,
  }))
}
