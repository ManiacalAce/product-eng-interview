import React from 'react';
import PropTypes from 'prop-types';
import { Card, Form, Radio } from "antd";

import "./MatrixCellEditor.css";


export default function MatrixCellEditor(props) {
  const { cellDataCategory, onChangeCellDataCategory } = props;

  const onChange = (e) => {
    if (onChangeCellDataCategory) {
      onChangeCellDataCategory(e.target.value);
    }
  };

  return (
    <Card
      title="Configure Matrix Cells"
      className="matrix-cell-editor"
    >
      <Form>
        <Form.Item name="m_m_data" label="MxM Data">
          <Radio.Group
            buttonStyle="solid"
            value={cellDataCategory}
            onChange={onChange}
            defaultValue={cellDataCategory}
          >
            <Radio.Button value="total_active_installs">Active Installs</Radio.Button>
            <Radio.Button value="total_uninstalls">Uninstalls</Radio.Button>
            <Radio.Button value="total_apps_reached">Apps Reached</Radio.Button>
          </Radio.Group>
        </Form.Item>

        <Form.Item name="m_n_data" label="MxN Data">
          <Radio.Group
            buttonStyle="solid"
            defaultValue="churn"
          >
            <Radio.Button value="churn" disabled>Churn</Radio.Button>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Card>
  );
}
MatrixCellEditor.propTypes = {
  cellDataCategory: PropTypes.string,
  onChangeCellDataCategory: PropTypes.func,
};
