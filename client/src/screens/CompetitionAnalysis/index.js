import React from "react";

import Heatmap from "../../components/Heatmap";
import MatrixEditor from "./MatrixEditor";
import MatrixCellEditor from "./MatrixCellEditor";
import * as mobileApi from "../../services/mobile";

import "./index.css"


export default class CompetitionAnalysis extends React.Component {

  static propTypes = {

  }

  constructor() {
    super();

    this.state = {
      sdkIds: [],
      matrixData: null,
      matrixCellDataCategory: "total_active_installs",
    };
  }

  render() {
    const { matrixData, matrixCellDataCategory } = this.state;

    return (
      <div className="competition-analysis">
        <section className="matrix-analysis-container">
          <article className="matrix-chart">
            {
              matrixData &&
                <Heatmap
                  series={matrixData}
                  width="100%"
                  height="100%"
                />
            }
          </article>

          <section className="matrix-analysis-config">
            <MatrixEditor onChangeActiveSDKs={this.handleChangeActiveSDKs} />

            <MatrixCellEditor
              cellDataCategory={matrixCellDataCategory}
              onChangeCellDataCategory={this.handleChangeMatrixCellDataCategory}
            />
          </section>
        </section>

      </div>
    );
  }

  fetchCompetitiveMatrix = async () => {
    if (!this.state.sdkIds) {
      return;
    }

    // TODO: Error handling
    const matrix = await mobileApi.getCompetitiveMatrix(this.state.sdkIds);
    const { matrixCellDataCategory } = this.state;

    // Keep a read-only reference to api-returned data
    this.rawMatrixData = matrix;

    this.setState({
      matrixData: this.convertMatrixToChartFormat(matrix, matrixCellDataCategory)
    });
  }

  handleChangeActiveSDKs = (sdkIds) => {
    this.setState({
      sdkIds: sdkIds,
    }, () => {
      this.fetchCompetitiveMatrix();
    });
  }

  handleChangeMatrixCellDataCategory = (cellDataCategory) => {
    if (!cellDataCategory) {
      return;
    }

    const newData = this.convertMatrixToChartFormat(this.rawMatrixData, cellDataCategory);
    this.setState({
      matrixData: newData,
      cellDataCategory: cellDataCategory,
    })
  }

  convertMatrixToChartFormat = (matrix, cellDataCategory) => {
    const chartData = [];

    for (const [sdkIdM, mInfo] of Object.entries(matrix)) {
      // Start off with ID. We'll get the name from the cell.
      let seriesName = sdkIdM;

      const seriesData = [];

      for (const [sdkIdN, nInfo] of Object.entries(mInfo)) {
        let xValue = null;
        let yValue = null;

        // TODO: Make yValue dynamic - let user pick what feature to show
        if (sdkIdM === sdkIdN) {
          // This is a MxM cell, with a particular SDK's info
          seriesName = nInfo["sdk_name"];
          xValue = nInfo["sdk_name"];
          yValue = nInfo[cellDataCategory];
        } else {
          // This is a MxN cell with a 'sdkIdM vs sdkIdN' comparison
          xValue = nInfo["to_sdk_name"]
          yValue = nInfo["churn"];
        }

        seriesData.push({
          x: xValue,
          y: yValue,
        })
      }

      chartData.push({
        name: seriesName,
        data: seriesData,
      });
    }

    return chartData;
  }

}
