import React from "react";
import PropTypes from "prop-types";
import { Button, Card, List } from "antd";

import SDKTag from "../../components/SDKTag";
import * as mobileApi from "../../services/mobile";
import SDKAutoComplete from "./SDKAutoComplete";

import "./MatrixEditor.css";


const initialSDKIds = [33, 875, 2081];


export default class MatrixEditor extends React.Component {

  static propTypes = {
    onChangeActiveSDKs: PropTypes.func,
  }

  constructor() {
    super();

    this.state = {
      isInEditMode: false,
      activeSDKs: [{name: "Paypal"}, {name: "Stripe"}],
    };
  }

  componentDidMount() {
    // Kick things off for starters, then the usual flow with user-interaction continues
    this.handleInitialActiveSDKs();
  }

  render() {
    return (
      <Card
        title="Configure Matrix"
        extra={this.renderPanelHeaderButtons()}
        className="matrix-editor"
      >
        {this.renderPanelBody()}
      </Card>
    );
  }

  renderPanelBody = () => {
    const { activeSDKs, isInEditMode } = this.state;

    return (
      <div className="panel-body">
        <div className="autocomplete-container">
          {
            isInEditMode &&
              <SDKAutoComplete
                onSelect={this.addActiveSDK}
              />
          }
        </div>

        <List
          size="small"
          renderItem={this.renderSDKListItem}
          dataSource={activeSDKs}
        />
      </div>
    );
  }

  renderPanelHeaderButtons = () => {
    const { isInEditMode } = this.state;

    let panelButtons = null;

    if (isInEditMode) {
      panelButtons = (
        <>
          <Button
            className="panel-button"
            type="primary"
            onClick={this.handleSaveEditModeChanges}
          >
            Save
          </Button>
          <Button
            className="panel-button"
            onClick={this.handleCancelEditModeChanges}
          >
            Cancel
          </Button>
        </>
      );
    } else {
      panelButtons = (
        <Button
          className="panel-button"
          type="primary"
          onClick={this.handleEnableEditMode}
        >
          Edit
        </Button>
      );
    }

    return (
      <div className="panel-actions">
        {panelButtons}
      </div>
    );
  }

  renderSDKListItem = (item) => {
    const { isInEditMode } = this.state;

    return (
      <List.Item>
        <SDKTag
          editable={isInEditMode}
          onClose={(event) => this.handleRemoveSDKTag(event, item.id)}
        >
          {item.name}
        </SDKTag>
      </List.Item>
    );
  }

  handleInitialActiveSDKs = async () => {
    const sdks = await mobileApi.getSDKs(initialSDKIds);

    this.setState(
      { activeSDKs: sdks },
      () => this.handleSaveEditModeChanges()
    );
  }

  handleSaveEditModeChanges = () => {
    const { onChangeActiveSDKs } = this.props;

    const sdkIds = this.state.activeSDKs.map(item => item.id);

    if (sdkIds.length === 0) {
      alert("Please specify at least one SDK");
      return;
    }

    if (onChangeActiveSDKs) {
      onChangeActiveSDKs(sdkIds);
    }

    this.setState({
      isInEditMode: false,
    })
  }

  handleEnableEditMode = () => {
    const { activeSDKs } = this.state;

    this.previousActiveSDKs = activeSDKs;
    this.setState({ isInEditMode: true });
  }

  handleCancelEditModeChanges = () => {
    this.setState({
      isInEditMode: false,
      activeSDKs: this.previousActiveSDKs,
    });
  }

  handleRemoveSDKTag = (event, sdkId) => {
    // Bypass the default uncontrolled removal-implementation in the lib
    event.preventDefault();

    const { activeSDKs } = this.state;

    this.setState({
      activeSDKs: activeSDKs.filter(item => item.id !== sdkId),
    });
  }

  addActiveSDK = (sdk) => {
    const existingItemIndex = this.state.activeSDKs.findIndex(item => item.id === sdk.id);
    if (existingItemIndex !== -1) {
      return;
    }

    this.setState({
      activeSDKs: [...this.state.activeSDKs, sdk],
    })
  }
}
