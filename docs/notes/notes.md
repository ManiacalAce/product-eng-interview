# Notes About the Problem

These are some miscellaneous bullet points extracted from my rough notes while thinking
about and working on this problem.

These aren't points about what 'should' be done, but more like a vocalization of thoughts
to aid my own understanding of things, and possible things to discuss.


## Approach to Visualizing This Data

- The provided matrix seems to pack a lot of data.
- Particularly notable is the way it shows 2 different 'kinds' of data:
  - The MxM cells show how many apps use the SDK
  - The MxN cells show the 'churn' from one SDK to another

- It seemed like quite a spoonful to swallow in the beginning, but after getting familiar
  with the problem-space, it feels like a nice compact representation of a good amount of
  data.
- Seems like something that would be appreciated by power-users or business-savvy folk who
  are already familiar with this specific visualization.

### Alternatives?

- Would there be any value in visualizing this differently? Say for:
  - Non power-users?
  - Particular consumers who we expect to have little time / short attention spans?

- How could this be visualized differently?
  - As a directed graph with different 'kinds' of links for different aspects of data?
    - But if the data is not sparse - ie. most nodes link to most other nodes, it would be
      a mess. In fact it would lead us back to this matrix repr which may be seen as a
      better option for that scenario.
  - Multi-widget view with different visualizations for different aspects?


## Information Inferrable from Provided Data

Just to summarize what seems to be extractable from the provided data-set.

### SDK Standalone Analysis

For each SDK M:

- Active Users:
  - No. of apps that currently have M installed
- Lost Users:
  - No. of apps that have used M before but now don't
- 'Reach':
  - Total no. of apps that have ever used M at some point (whether or not they have it
    installed right now)
- Potential Users:
  - No. of apps that have never used M


### SDK Comparison Analysis

For some SDK M vs another SDK N:

- "Churn":
  - No. of apps that have used M before but now use N. ie. M 'lost' these apps to N
    (presumably).
- Potential Users:
  - No. of apps that use N but have *never* used M
- 'Surveyors of the Space':
  - No. of apps that have used M AND N at some point.
    - Or take it further to find apps that have used a lot of players in the space. What
      are these guys looking for that they can't seem to find?

- Can we use app-ratings aggregations to make some conclusion about SDKs?
  - Too disconnected a data-point for meaningful conclusions?


## Potential Improvements on Current Data

### Capturing Installation Info

- How is the `installed` data-point sourced? It's periodic? How often? Can we capture time
  information and do some useful time-series analysis on it?

- also capture ratings-changes over periods, and see if 'after' SDK_X was installed, if
  the ratings deltas are moving upwards?


### Categorize SDKs for Better Analysis

- Does it make sense to build a competitive-matrix between SDKs when the SDKs aren't all
  from the same competition space?
  - Add and maintain `category_id` on `sdk`? Or m2m tags even if there's overlap in
    multiple categories.
- Data seems to have SDKs for payments, ads, analytics.


### Churn Calculation

- Churn calculation:
  - In the current dataset, if `app1` has 3 rows in the `app_sdk` table:
    - `app1|sdk1|installed=no`
    - `app1|sdk2|installed=yes`
    - `app1|sdk3|installed=yes`
  - In this case, it may be interpreted as app1 moving away from `sdk1` to both `sdk2` and
    `sdk3`.
    - Is this a 'gain' for BOTH `sdk2` and `sdk3`?
    - Or is this a bad comparison where `sdk1` should only be compared to ONE of `sdk2`
      or `sdk3`.

- We know this case happens since this query returns these scenarios:
  `select app_id, count(case when installed = true then 1 else null end) as active_install_count from app_sdk group by app_id having active_install_count > 1;`


### Schema / Queries

- Can we store the data in any other way to make it faster to run some of our queries?
  - Or just improve the 'churn' calculation queries in some way.


------------------------------------------------------------------------------------------

## Improvements in Current Code

A few missing features that I can implement if I take some more time:

- The `others` category in the matrix
- ~~For MxM cells, can show a couple other stats besides `total app count`. This is already
  returned in the API - just need to trigger the chart-render for it via UI.~~
- 'detail' views - get taken to another page when you click on any matrix cell:
  - For MxM cells, go to SDK-info screen, and show related data
  - For MxN cells, go to SDK-vs-SDK comparison screen and show related data there
  - Just needs some quick client-side routing setup, and then api calls to fetch apps etc
- Normalized values:
  - Need to sanity-check first if normalization works with the provided data, then easy to
    implement

The current solution is a quick implementation of the problem. If interested, these are
some of the things that would be improved during a production-implementation:

- Add missing validations in APIs and on client side. Try to never surprise a user with an
  error from an unhandled scenario.
- Try to optimize queries being done on backend - particularly see if the MxN cell-data
  calculations can be improved.
- After looking at subsequent requirements, see if backend API ought to be better designed
  as a 'primitives provider' rather than building up a complete bespoke
  competitive-matrix. The backend would provide building blocks, the frontend(s) would put
  them together into a matrix or whatever else.
- There's a 'glitch' currently where due to a library's behaviour,
  query-string-param-order maintained in the app is messed up during serialization. It
  currently sometimes leads to a different ordering in the matrix vs the matrix-editor
  tag-list. This is mildly annoying for UX.
- Matrix-fetch GET endpoint accepts `sdk_ids` in query string. If we plan to build huge
  matrices for whatever reason, change endpoint to POST to bypass url-size limits.
- Verify the UI works on different screens. I used `rems` as units for the most part, but
  there's some `%` values at play in some places, and from some library code. Gotta ensure
  there's no surprises on big screens.
- Set up proper config wrappers, secrets management to fetch sensitive stuff from env-vars
- Consider using TypeScript on frontend to remove the need for a whole class of
  unit-tests and nip a lot of errors in the bud. In my experience it has helped when
  working on massive frontend codebases.
- General cleanup of code for maintenance. (refactor, add docstrings, use css-variables,
  etc etc)

