from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import mobile.api


api_app = FastAPI()


api_app.include_router(mobile.api.router)


allowed_origins = [
    "http://localhost:3000",
]
api_app.add_middleware(
    CORSMiddleware,
    allow_origins=allowed_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
