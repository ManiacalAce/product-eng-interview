import sqlalchemy as sa
import sqlalchemy.ext.declarative
import sqlalchemy.orm


# TODO: Fetch from env/config
SQLALCHEMY_DB_URL = "sqlite:///./data/data.db"


engine = sa.create_engine(
    SQLALCHEMY_DB_URL,

    # Needed for sqlite
    connect_args={
        "check_same_thread": False,
    }
)


DatabaseScope = sqlalchemy.orm.sessionmaker(
    autocommit=False, autoflush=False, bind=engine
)


BaseModel = sqlalchemy.ext.declarative.declarative_base()


def get_db_session():
    try:
        db = DatabaseScope()
        yield db
    finally:
        db.close()
