import uvicorn

from app.app import api_app  # noqa


if __name__ == "__main__":
    uvicorn.run(
        "main:api_app", host="localhost", port=5000,
        log_level="info", reload=True
    )
