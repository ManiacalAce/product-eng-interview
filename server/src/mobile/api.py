from fastapi import APIRouter, Depends, Query
import typing

from app.database import get_db_session

from . import crud


router = APIRouter()


@router.get("/ping")
def ping():
    return f"Pong!"


@router.get("/sdk")
def get_sdk_list(
    db_session=Depends(get_db_session),
    sdk_ids: typing.List[int] = Query([]),
    q: str = Query(None),
    limit: int = Query(None),
):
    return crud.get_sdk_list(db_session, sdk_ids=sdk_ids, q=q, limit=limit)


@router.get("/sdk/competitive-matrix")
def get_sdk_competitive_matrix(
    db_session=Depends(get_db_session),
    sdk_ids: typing.List[int] = Query(...),
):
    return crud.get_sdk_competitive_matrix(db_session, sdk_ids)
