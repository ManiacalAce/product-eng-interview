import collections

import sqlalchemy as sa

from . import models


def get_sdk_competitive_matrix(db_session, sdk_ids):
    """

    """
    if not sdk_ids:
        raise ValueError("At least one `sdk_id` must be provided")

    matrix = collections.defaultdict(dict)

    # Get MxM info
    sdk_distribution_analysis = get_sdk_distribution_analysis(db_session, sdk_ids)
    sdk_distribution_analysis_map = dict(
        (distribution_info.sdk_id, distribution_info)
        for distribution_info in sdk_distribution_analysis
    )

    # Get MxN info
    sdk_competition_analysis = get_sdk_competition_analysis(db_session, sdk_ids)

    # Grab SDK names. Likely faster (and simpler) to get this with a small separate query
    # rather than adding joins on the other larger queries.
    sdks = get_sdk_list(db_session, sdk_ids)
    sdk_map = dict((sdk.id, sdk) for sdk in sdks)

    # Construct the relevant cells
    for sdk_i in sdk_ids:
        for sdk_j in sdk_ids:
            if sdk_i == sdk_j:
                distribution_info = sdk_distribution_analysis_map[sdk_i]
                cell = {
                    "sdk_id": sdk_i,
                    "sdk_name": sdk_map[sdk_i].name,
                    **distribution_info._asdict(),
                }
            else:
                competition_info = sdk_competition_analysis[sdk_i][sdk_j]
                cell = {
                    "from_sdk_id": sdk_i,
                    "from_sdk_name": sdk_map[sdk_i].name,
                    "to_sdk_id": sdk_j,
                    "to_sdk_name": sdk_map[sdk_j].name,
                    **competition_info,
                }

            matrix[sdk_i][sdk_j] = cell

    return matrix


def get_sdk_distribution_analysis(db_session, sdk_ids):
    """

    """
    if not sdk_ids:
        raise ValueError("At least one `sdk_id` must be provided")

    # The no. of apps that ever used a given SDK
    case_total_apps_reached = sa.func.count(
        models.AppSDKLink.app_id
    ).label("total_apps_reached")

    # The no. of apps that currently have a given SDK installed
    case_total_active_installs = sa.func.count(
        sa.case(
            [
                [models.AppSDKLink.installed.is_(True), True]
            ],
            else_=sa.sql.expression.null()
        )
    ).label("total_active_installs")

    # The no. of apps that do NOT have a particular SDK installed (but possibly used to)
    case_total_uninstalls = sa.func.count(
        sa.case(
            [
                [models.AppSDKLink.installed.is_(False), True]
            ],
            else_=sa.sql.expression.null()
        )
    ).label("total_uninstalls")

    return db_session.query(
        models.AppSDKLink.sdk_id,
        case_total_apps_reached,
        case_total_active_installs,
        case_total_uninstalls,
    ).group_by(
        models.AppSDKLink.sdk_id
    ).having(
        models.AppSDKLink.sdk_id.in_(sdk_ids)
    ).all()


def get_sdk_competition_analysis(db_session, sdk_ids):
    """

    """
    competition_analysis = collections.defaultdict(dict)

    # TODO: See if the following can be optimized without doing this many queries. Right
    # now there's `m^2 - m` queries.
    for sdk_id_m in sdk_ids:
        for sdk_id_n in sdk_ids:
            if sdk_id_m == sdk_id_n:
                continue

            apps_that_ever_had_m = db_session.query(models.AppSDKLink.app_id).filter(
                models.AppSDKLink.sdk_id == sdk_id_m
            ).subquery()

            # Apps that had M installed at some point, but now have an active N
            # installtion
            churn_m_to_n = db_session.query(models.AppSDKLink).filter(
                models.AppSDKLink.app_id.in_(apps_that_ever_had_m),
                models.AppSDKLink.sdk_id == sdk_id_n,
                models.AppSDKLink.installed.is_(True),
            ).count()

            # TODO: See what else could be added
            analysis = {
                "churn": churn_m_to_n,
            }

            competition_analysis[sdk_id_m][sdk_id_n] = analysis

    return competition_analysis


def get_sdk_list(db_session, sdk_ids=None, q=None, limit=50):
    """

    """
    filters = []

    if sdk_ids:
        filters.append(models.SDK.id.in_(sdk_ids))

    if q:
        filters.append(models.SDK.name.ilike(f"%{q}%"))

    query = db_session.query(models.SDK).filter(*filters)

    if limit is not None:
        query = query.limit(limit)

    return query.all()
