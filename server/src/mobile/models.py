import sqlalchemy as sa
import sqlalchemy_utils

from app.database import BaseModel


@sqlalchemy_utils.generic_repr
class App(BaseModel):
    __tablename__ = "app"

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.Text)
    company_url = sa.Column(sa.Text)
    release_date = sa.Column(sa.Date)
    genre_id = sa.Column(sa.Integer)
    artwork_large_url = sa.Column(sa.Text)
    seller_name = sa.Column(sa.Text)
    five_star_ratings = sa.Column(sa.Integer)
    four_star_ratings = sa.Column(sa.Integer)
    three_star_ratings = sa.Column(sa.Integer)
    two_star_ratings = sa.Column(sa.Integer)
    one_star_ratings = sa.Column(sa.Integer)


@sqlalchemy_utils.generic_repr
class SDK(BaseModel):
    __tablename__ = "sdk"

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.Text)
    slug = sa.Column(sa.Text)
    url = sa.Column(sa.Text)
    description = sa.Column(sa.Text)


@sqlalchemy_utils.generic_repr
class AppSDKLink(BaseModel):
    __tablename__ = "app_sdk"

    app_id = sa.Column(sa.Integer, sa.ForeignKey(App.id), primary_key=True)
    sdk_id = sa.Column(sa.Integer, sa.ForeignKey(SDK.id), primary_key=True)
    installed = sa.Column(sa.Boolean)
