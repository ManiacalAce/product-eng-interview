# Solution - Competitive Matrix


## Implementation Summary

This repo contains both the backend and the frontend implementations stored in `server/`
and `client/` respectively.

On the backend, we use:
- `poetry` for dependency management
- `FastAPI` with `uvicorn` for our api-server implementation
- `SQLAlchemy` for the DB-interaction-layer

On the frontend, we use:
- `React` - bootstrapped from the `create-react-app` scaffolding
- `ApexCharts` as the choice for our heatmap chart
- `antd` components just to have the base for a few pre-styled components so time can be
  spent on feature-implementation.


## Installation Instructions

While I did read about `nix`, and do find it intruiging, I'm specifying more 'manual'
instructions here in the interest of allocating time to implementation-related stuff.

### Prerequisites

Please ensure the following software is available on your machine.

- `poetry` for Python
  - https://github.com/python-poetry/poetry
  - It will automatically set up a venv, and manage dependencies

- `nodejs` and `npm` for JavaScript
  - https://docs.npmjs.com/downloading-and-installing-node-js-and-npm


### Installation


#### Step 1 - Clone the repo into a folder

```bash

$ git clone git@gitlab.com:ManiacalAce/product-eng-interview.git mixrank_demo

```


#### Step 2 - Prepare Backend Environment


```bash

$ cd mixrank_demo/server
$ poetry install

```

We should now have a new virtual-environment automatically created, with all relevant
dependencies installed in it.


#### Step 3 - Run Backend Server

```bash

$ poetry run python src/main.py

```

This will run the specified file in the context of our project's virtual environment.

The server runs on port `5000` of localhost as specified in the `src/main.py` file. Any
change to this will also need changes to the CORS config, and the frontend config.


#### Step 4 - Prepare Frontend Environment

```bash

$ cd mixrank_demo/client
$ npm install

```


#### Step 5 - Run Frontend App

```bash

$ npm start

```

This will run the frontend webapp on port `3000` on localhost. It will communicate with
the backend running on port `5000`.


## Implementation Notes

Notes about this solution are in the `docs` directory [here](./docs/notes/notes.md).
